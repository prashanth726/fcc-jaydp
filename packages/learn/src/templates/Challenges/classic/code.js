import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { ReflexContainer, ReflexSplitter, ReflexElement } from 'react-reflex';
import { graphql } from 'gatsby';

import Editor from './Editor';
import Preview from '../components/Preview';
import SidePanel from '../components/Side-Panel';
import Output from '../components/Output';
import CompletionModal from '../components/CompletionModal';
import HelpModal from '../components/HelpModal';
import VideoModal from '../components/VideoModal';
import ResetModal from '../components/ResetModal';

import { randomCompliment } from '../utils/get-words';
import { createGuideUrl } from '../utils';
import { challengeTypes } from '../../../../utils/challengeTypes';
import { ChallengeNode } from '../../../redux/propTypes';
import { dasherize } from '../../../../utils';
import {
  createFiles,
  challengeFilesSelector,
  challengeTestsSelector,
  challengeIdSelector,
  initTests,
  updateChallengeMeta,
  challengeMounted,
  updateSuccessMessage,
  consoleOutputSelector
} from '../redux';

import './classic.css';
import '../components/test-frame.css';

import decodeHTMLEntities from '../../../../utils/decodeHTMLEntities';

const selector = createSelector(
  [
    challengeFilesSelector,
    challengeTestsSelector,
    challengeIdSelector,
    consoleOutputSelector
  ],
  (files, tests, id, output) => ({
    files,
    tests,
    id,
    output
  })
);

const mapStateToProps = (state, props) => {
  return selector(state, props);
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createFiles,
      initTests,
      updateChallengeMeta,
      challengeMounted,
      updateSuccessMessage
    },
    dispatch
  );

const propTypes = {
  challengeMounted: PropTypes.func.isRequired,
  createFiles: PropTypes.func.isRequired,
  data: PropTypes.shape({
    challengeNode: ChallengeNode
  }),
  files: PropTypes.shape({
    key: PropTypes.string
  }),
  initTests: PropTypes.func.isRequired,
  output: PropTypes.string,
  pageContext: PropTypes.shape({
    challengeMeta: PropTypes.shape({
      nextchallengePath: PropTypes.string
    })
  }),
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      testString: PropTypes.string
    })
  ),
  updateChallengeMeta: PropTypes.func.isRequired,
  updateSuccessMessage: PropTypes.func.isRequired
};

class Code extends PureComponent {
  static defaultProps = {
    data: {
      challengeNode: {
        title: 'Say Hello to HTML Elements',
        description: [
          'To pass the test on this challenge, change your <code>h1</code> element\'s text to say "Hello World".'
        ],
        challengeType: 0,
        videoUrl: 'https://scrimba.com/p/pVMPUv/cE8Gpt2',
        fields: {
          slug:
            '/responsive-web-design/basic-html-and-html5/say-hello-to-html-elements',
          blockName: 'Basic HTML and HTML5',
          tests: [
            {
              text:
                'Your <code>h1</code> element should have the text "Hello World".',
              testString:
                "assert.isTrue((/hello(\\s)+world/gi).test($('h1').text()), 'Your <code>h1</code> element should have the text \"Hello World\".');"
            },
            {
              text:
                'Your <code>h1</code> element should have the text "Hello World".',
              testString:
                "assert.isTrue((/hello(\\s)+world/gi).test($('h1').text()), 'Your <code>h1</code> element should have the text \"Hello World\".');"
            }
          ]
        },
        required: [],
        files: {
          indexhtml: {
            key: 'indexhtml',
            ext: 'html',
            name: 'index',
            contents: '<h1>Hello</h1>',
            head: '',
            tail: ''
          },
          indexjs: null,
          indexjsx: null
        }
      }
    },
    pageContext: {
      challengeMeta: {
        id: 'bad87fee1348bd9aedf0887a',
        introPath: '',
        nextChallengePath:
          '/responsive-web-design/basic-html-and-html5/inform-with-the-paragraph-element',
        required: [],
        template: null
      },
      slug:
        '/responsive-web-design/basic-html-and-html5/headline-with-the-h2-element'
    }
  };
  constructor(defaultProps) {
    super(defaultProps);
    console.log('sds', this.props);
    this.resizeProps = {
      onStopResize: this.onStopResize.bind(this),
      onResize: this.onResize.bind(this)
    };

    this.state = {
      resizing: false
    };
  }

  onResize() {
    this.setState({ resizing: true });
  }

  onStopResize() {
    this.setState({ resizing: false });
  }

  componentDidMount() {
    console.log('Uuu', this.props);
    const {
      challengeMounted,
      createFiles,
      initTests,
      updateChallengeMeta,
      updateSuccessMessage,
      data: {
        challengeNode: {
          files,
          title,
          fields: { tests },
          challengeType
        }
      },
      pageContext: { challengeMeta },
      challengeId
    } = this.props;
    createFiles({ files, challengeId });
    initTests({ tests, challengeId });
    updateChallengeMeta({
      ...challengeMeta,
      title,
      challengeType,
      challengeId
    });
    updateSuccessMessage(randomCompliment());
    challengeMounted(challengeMeta.id);
  }

  componentDidUpdate(prevProps) {
    const {
      data: {
        challengeNode: { title: prevTitle }
      }
    } = prevProps;
    const {
      challengeMounted,
      createFiles,
      initTests,
      updateChallengeMeta,
      updateSuccessMessage,
      data: {
        challengeNode: {
          files,
          title: currentTitle,
          fields: { tests },
          challengeType
        }
      },
      pageContext: { challengeMeta },
      challengeId
    } = this.props;
    if (prevTitle !== currentTitle) {
      updateSuccessMessage(randomCompliment());
      createFiles({ files, challengeId });
      initTests({ tests, challengeId });
      updateChallengeMeta({
        ...challengeMeta,
        title: currentTitle,
        challengeType
      });
      challengeMounted(challengeMeta.id);
    }
  }

  render() {
    const {
      data: {
        challengeNode: {
          challengeType,
          fields: { blockName, slug },
          title,
          description,
          videoUrl
        }
      },
      files,
      output,
    } = this.props;
    {
      console.log('file', this.props);
    }
    const editors = Object.keys(files)
      .map(key => files[key])
      .map((file, index) => (
        <ReflexContainer key={file.key + index} orientation="horizontal">
          {index !== 0 && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          <ReflexElement
            flex={1}
            propagateDimensions={true}
            renderOnResize={true}
            renderOnResizeRate={20}
            {...this.resizeProps}
          >
            <Editor {...file} fileKey={file.key} />
          </ReflexElement>
          {index + 1 === Object.keys(files).length && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          {index + 1 === Object.keys(files).length ? (
            <ReflexElement
              flex={0.25}
              propagateDimensions={true}
              renderOnResize={true}
              renderOnResizeRate={20}
              {...this.resizeProps}
            >
              <Output
                defaultOutput={`
/**
* Your test output will go here.
*/
`}
                output={decodeHTMLEntities(output)}
              />
            </ReflexElement>
          ) : null}
        </ReflexContainer>
      ));
    const showPreview =
      challengeType === challengeTypes.html ||
      challengeType === challengeTypes.modern;
    const blockNameTitle = `${blockName}: ${title}`;
    return (
      <Fragment>
        <Helmet title={`${blockNameTitle} | Learn freeCodeCamp`} />
        <ReflexContainer orientation="vertical">
          <ReflexElement flex={1} {...this.resizeProps}>
            <SidePanel
              className="full-height"
              description={description}
              guideUrl={createGuideUrl(slug)}
              section={dasherize(blockName)}
              title={blockNameTitle}
              videoUrl={videoUrl}
              challengeId={this.props.challengeId}
            />
          </ReflexElement>
          <ReflexSplitter propagate={true} {...this.resizeProps} />
          <ReflexElement flex={1} {...this.resizeProps}>
            {editors}
          </ReflexElement>
          {showPreview && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          {showPreview ? (
            <ReflexElement flex={0.7} {...this.resizeProps}>
              <Preview
                className="full-height"
                disableIframe={this.state.resizing}
              />
            </ReflexElement>
          ) : null}
        </ReflexContainer>

        <CompletionModal />
        <HelpModal />
        <VideoModal videoUrl={videoUrl} />
        <ResetModal />
      </Fragment>
    );
  }
}

Code.displayName = 'ShowClassic';
Code.propTypes = propTypes;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Code);

// export const query = graphql`
//   query ClassicChallenge($slug: String!) {
//     challengeNode(fields: { slug: { eq: $slug } }) {
//       title
//       description
//       challengeType
//       videoUrl
//       fields {
//         slug
//         blockName
//         tests {
//           text
//           testString
//         }
//       }
//       required {
//         link
//         raw
//         src
//       }
//       files {
//         indexhtml {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//         indexjs {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//         indexjsx {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//       }
//     }
//   }
// `;
