import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { createSelector } from 'reselect';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { ReflexContainer, ReflexSplitter, ReflexElement } from 'react-reflex';
import { graphql } from 'gatsby';

import Editor from './Editor';
import Preview from '../components/Preview';
import SidePanel from '../components/Side-Panel';
import Output from '../components/Output';
import CompletionModal from '../components/CompletionModal';
import HelpModal from '../components/HelpModal';
import VideoModal from '../components/VideoModal';
import ResetModal from '../components/ResetModal';

import { randomCompliment } from '../utils/get-words';
import { createGuideUrl } from '../utils';
import { challengeTypes } from '../../../../utils/challengeTypes';
import { ChallengeNode } from '../../../redux/propTypes';
import { dasherize } from '../../../../utils';
import {
  createFiles,
  challengeFilesSelector,
  challengeTestsSelector,
  initTests,
  updateChallengeMeta,
  challengeMounted,
  updateSuccessMessage,
  consoleOutputSelector
} from '../redux';

import './classic.css';
import '../components/test-frame.css';

import decodeHTMLEntities from '../../../../utils/decodeHTMLEntities';

const mapStateToProps = createSelector(
  challengeFilesSelector,
  challengeTestsSelector,
  consoleOutputSelector,
  (files, tests, output) => ({
    files,
    tests,
    output
  })
);

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      createFiles,
      initTests,
      updateChallengeMeta,
      challengeMounted,
      updateSuccessMessage
    },
    dispatch
  );

const propTypes = {
  challengeMounted: PropTypes.func.isRequired,
  createFiles: PropTypes.func.isRequired,
  data: PropTypes.shape({
    challengeNode: ChallengeNode
  }),
  files: PropTypes.shape({
    key: PropTypes.string
  }),
  initTests: PropTypes.func.isRequired,
  output: PropTypes.string,
  pageContext: PropTypes.shape({
    challengeMeta: PropTypes.shape({
      nextchallengePath: PropTypes.string
    })
  }),
  tests: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      testString: PropTypes.string
    })
  ),
  updateChallengeMeta: PropTypes.func.isRequired,
  updateSuccessMessage: PropTypes.func.isRequired
};

class Test extends PureComponent {

    static defaultProps= {
        "data": {
          "challengeNode": {
            "title": "Say Hello to HTML Elements",
            "description": [
              "Welcome to freeCodeCamp's HTML coding challenges. These will walk you through web development step-by-step.",
              "First, you'll start by building a simple web page using HTML. You can edit <code>code</code> in your <code>code editor</code>, which is embedded into this web page.",
              "Do you see the code in your code editor that says <code>&#60;h1&#62;Hello&#60;/h1&#62;</code>? That's an HTML <code>element</code>.",
              "Most HTML elements have an <code>opening tag</code> and a <code>closing tag</code>.",
              "Opening tags look like this:",
              "<code>&#60;h1&#62;</code>",
              "Closing tags look like this:",
              "<code>&#60;/h1&#62;</code>",
              "The only difference between opening and closing tags is the forward slash after the opening bracket of a closing tag.",
              "Each challenge has tests you can run at any time by clicking the \"Run tests\" button. When you pass all tests, you'll be prompted to submit your solution and go to the next coding challenge.",
              "<hr>",
              "To pass the test on this challenge, change your <code>h1</code> element's text to say \"Hello World\"."
            ],
            "challengeType": 0,
            "videoUrl": "https://scrimba.com/p/pVMPUv/cE8Gpt2",
            "fields": {
              "slug": "/responsive-web-design/basic-html-and-html5/say-hello-to-html-elements",
              "blockName": "Basic HTML and HTML5",
              "tests": [
                {
                  "text": "Your <code>h1</code> element should have the text \"Hello World\".",
                  "testString": "assert.isTrue((/hello(\\s)+world/gi).test($('h1').text()), 'Your <code>h1</code> element should have the text \"Hello World\".');"
                }
              ]
            },
            "required": [],
            "files": {
              "indexhtml": {
                "key": "indexhtml",
                "ext": "html",
                "name": "index",
                "contents": "<h1>Hello</h1>",
                "head": "",
                "tail": ""
              },
              "indexjs": null,
              "indexjsx": null
            }
          }
        },
        pageContext:{ challengeMeta: { id: "bad87fee1348bd9aedf0887a",
        introPath: "",
        nextChallengePath: "/responsive-web-design/basic-html-and-html5/inform-with-the-paragraph-element",
        required: [],
        template: null }, slug: "/responsive-web-design/basic-html-and-html5/headline-with-the-h2-element"}
      }
  constructor(defaultProps) {
    super(defaultProps);
console.log("scvv",this.props.tes)
console.log("ddfdf",this.props)
    this.resizeProps = {
      onStopResize: this.onStopResize.bind(this),
      onResize: this.onResize.bind(this)
    };

    this.state = {
      resizing: false
    };
  }

  onResize() {
    this.setState({ resizing: true });
  }

  onStopResize() {
    this.setState({ resizing: false });
  }

  componentDidMount() {
    const {
      challengeMounted,
      createFiles,
      initTests,
      updateChallengeMeta,
      updateSuccessMessage,
      data: {
        challengeNode: { files, fields: challengeType }
      },
      tes,
      title,
      pageContext: { challengeMeta }
    } = this.props;

    var testss = [JSON.parse(tes)].reduce((accumulator, currentValue, currentIndex, array)=> {
        console.log("dfvf",array)
        var length = array[0].text.length;
        for(var i=0;i<length;i++){
        var c = {text:array[currentIndex].text[i], testString:array[currentIndex].testString[i]}
        accumulator.push(c)
        }
        return accumulator;
        },[])
console.log("heh",testss)
    createFiles(files);
    initTests(testss);
    updateChallengeMeta({ ...challengeMeta, title, challengeType });
    updateSuccessMessage(randomCompliment());
    challengeMounted(challengeMeta.id);
  }

  componentDidUpdate(prevProps) {
    const { data: { challengeNode: { title: prevTitle } } } = prevProps;
    const {
      challengeMounted,
      createFiles,
      initTests,
      updateChallengeMeta,
      updateSuccessMessage,
      data: {
        challengeNode: {
          files,
          title: currentTitle,
          challengeType
        }
      },
      tes,
      pageContext: { challengeMeta }
    } = this.props;
    if (prevTitle !== currentTitle) {
        var testss = [JSON.parse(tes)].reduce((accumulator, currentValue, currentIndex, array)=> {
            var length = array[0].text.length;
            for(var i=0;i<length;i++){
            var c = {text:array[currentIndex].text[i], testString:array[currentIndex].testString[i]}
            accumulator.push(c)
            }
            return accumulator;
            },[])
      updateSuccessMessage(randomCompliment());
      createFiles(files);
      initTests(testss);
      updateChallengeMeta({
        ...challengeMeta,
        title: currentTitle,
        challengeType
      });
      challengeMounted(challengeMeta.id);
    }
  }

  render() {
    const {
      data: {
        challengeNode: {
          challengeType,
          fields: { blockName, slug },
          description,
          videoUrl
        }
      },
      title,
      files,
      output
    } = this.props;
    const editors = Object.keys(files)
      .map(key => files[key])
      .map((file, index) => (
        <ReflexContainer key={file.key + index} orientation='horizontal'>
          {index !== 0 && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          <ReflexElement
            flex={1}
            propagateDimensions={true}
            renderOnResize={true}
            renderOnResizeRate={20}
            {...this.resizeProps}
            >
            <Editor {...file} fileKey={file.key} />
          </ReflexElement>
          {index + 1 === Object.keys(files).length && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          {index + 1 === Object.keys(files).length ? (
            <ReflexElement
              flex={0.25}
              propagateDimensions={true}
              renderOnResize={true}
              renderOnResizeRate={20}
              {...this.resizeProps}
              >
              <Output
                defaultOutput={`
/**
* Your test output will go here.
*/
`}
                output={decodeHTMLEntities(output)}
              />
            </ReflexElement>
          ) : null}
        </ReflexContainer>
      ));
    const showPreview =
      challengeType === challengeTypes.html ||
      challengeType === challengeTypes.modern;
    const blockNameTitle = `${blockName}: ${title}`;
    return (
      <Fragment>
        <Helmet title={`${blockNameTitle} | Learn freeCodeCamp`} />
        <ReflexContainer orientation='vertical'>
          <ReflexElement flex={1} {...this.resizeProps}>
            <SidePanel
              className='full-height'
              description={description}
              guideUrl={createGuideUrl(slug)}
              section={dasherize(blockName)}
              title={blockNameTitle}
              videoUrl={videoUrl}
            />
          </ReflexElement>
          <ReflexSplitter propagate={true} {...this.resizeProps} />
          <ReflexElement flex={1} {...this.resizeProps}>
            {editors}
          </ReflexElement>
          {showPreview && (
            <ReflexSplitter propagate={true} {...this.resizeProps} />
          )}
          {showPreview ? (
            <ReflexElement flex={0.7} {...this.resizeProps}>
              <Preview
                className='full-height'
                disableIframe={this.state.resizing}
              />
            </ReflexElement>
          ) : null}
        </ReflexContainer>

        <CompletionModal />
        <HelpModal />
        <VideoModal videoUrl={videoUrl}/>
        <ResetModal />
      </Fragment>
    );
  }
}

Test.displayName = 'ShowClassic';
Test.propTypes = propTypes;

export default connect(mapStateToProps, mapDispatchToProps)(Test);

// export const query = graphql`
//   query TestChallenge {
//     challengeNode(fields: { slug: { eq: "/responsive-web-design/basic-html-and-html5/say-hello-to-html-elements" } }) {
//       title
//       description
//       challengeType
//       videoUrl
//       fields {
//         slug
//         blockName
//         tests {
//           text
//           testString
//         }
//       }
//       required {
//         link
//         raw
//         src
//       }
//       files {
//         indexhtml {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//         indexjs {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//         indexjsx {
//           key
//           ext
//           name
//           contents
//           head
//           tail
//         }
//       }
//     }
//   }
// `;
