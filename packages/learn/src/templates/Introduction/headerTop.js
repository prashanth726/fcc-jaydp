
/*eslint-disable */
import React, { Fragment } from 'react';
import './headerTop.css';


import {Row, Col, Layout, Menu, Icon, Timeline,Steps } from 'antd';
import { relative } from 'upath';

const {  Header } = Layout;


export default function HeaderTop() {
    return (
        <header id="header">
        <Row>
          <Col xxl={4} xl={5} lg={5} md={6} sm={24} xs={24}>
            <a id="logo">
              <img style={{height: 32,marginRight: 16}} alt="logo" src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" />
              <img style={{height: 16,position:relative,top:1}} alt="Ant Design" src="https://gw.alipayobjects.com/zos/rmsportal/DkKNubTaaVsKURhcVGkh.svg" />
        </a>
          </Col>
          <Col xxl={20} xl={19} lg={19} md={18} sm={0} xs={0}>
           
          </Col>
        </Row>

  </header>
  );
}