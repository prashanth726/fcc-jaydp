import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link, graphql, navigate } from 'gatsby';
import Helmet from 'react-helmet';
import { Button, ListGroup, ListGroupItem } from 'react-bootstrap';
import rehypeReact from "rehype-react"
import FullWidthRow from '../../components/util/FullWidthRow';
import ButtonSpacer from '../../components/util/ButtonSpacer';
import { MarkdownRemark, AllChallengeNode } from '../../redux/propTypes';
import { Alert } from 'antd';

import Counter from "../../components/util/Counter"

import Test from "../Challenges/classic/Test"

import './intro.css';

import { Layout, Menu, Breadcrumb, Icon } from 'antd';


const { SubMenu } = Menu;
const { Header, Content, Sider } = Layout;

const renderAst = new rehypeReact({
  createElement: React.createElement,
  components: { "interactive-counter": Counter,
  "test-com": Test },
}).Compiler

const propTypes = {
  data: PropTypes.shape({
    markdownRemark: MarkdownRemark,
    allChallengeNode: AllChallengeNode
  })
};

function renderMenuItems({ edges = [] }) {
  return edges.map(({ node }) => node).map(({ title, fields: { slug } }) => (
    <Link key={'intro-' + slug} to={slug}>
      <ListGroupItem>{title}</ListGroupItem>
    </Link>
  ));
}

function handleCurriculumClick() {
  return navigate('/');
}

function IntroductionPage({ data: { markdownRemark, allChallengeNode } }) {
  const { htmlAst, frontmatter: { block } } = markdownRemark;
  console.log(htmlAst.html)
  const firstLesson = allChallengeNode && allChallengeNode.edges[0].node;
  const firstLessonPath = firstLesson
    ? firstLesson.fields.slug
    : '/strange-place';
  return (
    <Fragment>
      <Helmet>
        <title>{block} | freeCodeCamp</title>
      </Helmet>

<Layout>
    <Header className="header">
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="2">nav 2</Menu.Item>
        <Menu.Item key="3">nav 3</Menu.Item>
      </Menu>
    </Header>
    <Layout>
      <Sider width={250} style={{ background: '#fff' }}  breakpoint="lg"
      collapsedWidth="0">
        <Menu
          mode="inline"
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%', borderRight: 0 }}
        >
          <SubMenu key="sub1" title={<span><Icon type="user" />subnav 1</span>}>
            <Menu.Item key="1">option1</Menu.Item>
            <Menu.Item key="2">option2</Menu.Item>
            <Menu.Item key="3">option3</Menu.Item>
            <Menu.Item key="4">option4</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" title={<span><Icon type="laptop" />subnav 2</span>}>
            <Menu.Item key="5">option5</Menu.Item>
            <Menu.Item key="6">option6</Menu.Item>
            <Menu.Item key="7">option7</Menu.Item>
            <Menu.Item key="8">option8</Menu.Item>
          </SubMenu>
          <SubMenu key="sub3" title={<span><Icon type="notification" />subnav 3</span>}>
            <Menu.Item key="9">option9</Menu.Item>
            <Menu.Item key="10">option10</Menu.Item>
            <Menu.Item key="11">option11</Menu.Item>
            <Menu.Item key="12">option12</Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 24px 24px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 280 }}>
        <div>{renderAst(htmlAst)}</div>
        </Content>
      </Layout>
    </Layout>
  </Layout>
 

      <FullWidthRow>
     
      <Alert message="Success Text" type="success" />
      </FullWidthRow>
      <FullWidthRow>
        <Link className='btn btn-lg btn-primary btn-block' to={firstLessonPath}>
          Go to the first lesson
        </Link>
        <ButtonSpacer />
        <Button
          block={true}
          bsSize='lg'
          className='btn-primary-invert'
          onClick={handleCurriculumClick}
          >
          View the curriculum
        </Button>
        <ButtonSpacer />
        <hr />
      </FullWidthRow>
      <FullWidthRow>
        <h2 className='intro-toc-title'>Upcoming Lessons</h2>
        <ListGroup className='intro-toc'>
          {allChallengeNode ? renderMenuItems(allChallengeNode) : null}
        </ListGroup>
      </FullWidthRow>
    </Fragment>
  );
}

IntroductionPage.displayName = 'IntroductionPage';
IntroductionPage.propTypes = propTypes;

export default IntroductionPage;

export const query = graphql`
  query IntroPageBySlug($slug: String!, $block: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        block
      }
      htmlAst
    }
    allChallengeNode(
      filter: { block: { eq: $block } }
      sort: { fields: [superOrder, order, suborder] }
    ) {
      edges {
        node {
          fields {
            slug
          }
          title
        }
      }
    }
  }
`;
