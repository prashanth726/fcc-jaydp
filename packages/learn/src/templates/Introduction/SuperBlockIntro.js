/*eslint-disable */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { graphql } from 'gatsby';
import './intro.css';
// import FullWidthRow from '../../components/util/FullWidthRow';
import { MarkdownRemark } from '../../redux/propTypes';
import HeaderTop from "./headerTop";

import Code from "../Challenges/classic/code"


// import { Layout,Row, Col,Button, Menu, Breadcrumb, Icon } from 'antd';

// const { Header, Content, Footer, Sider } = Layout;

import {Row, Col, Layout, Menu, Icon, Timeline,Steps, Input } from 'antd';

const { Content, Footer, Sider } = Layout;
const Step = Steps.Step;
const { SubMenu } = Menu;
const Search = Input.Search;

const propTypes = {
  data: PropTypes.shape({
    markdownRemark: MarkdownRemark
  })
};

function SuperBlockIntroductionPage({ data: { markdownRemark } }) {
 
  const { html, frontmatter: { superBlock } } = markdownRemark;
  return (
    <Fragment>
      <Helmet>
        <title>Hey {superBlock} | freeCodeCamp</title>
      </Helmet>

  {/* <Layout>
    <Header className="header">
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="2">nav 2</Menu.Item>
        <Menu.Item key="3">nav 3</Menu.Item>
      </Menu>
    </Header>
    <Content >
     
      <Layout style={{ background: '#fff'} }>
        
        <Content style={{ padding: '0 24px', minHeight: 280 }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Home</Breadcrumb.Item>
        <Breadcrumb.Item>List</Breadcrumb.Item>
        <Breadcrumb.Item>App</Breadcrumb.Item>
      </Breadcrumb>
          <h1>Content</h1>
        </Content>
        <Sider breakpoint="lg"
      collapsedWidth="0" width={200} reverseArrow={true} trigger={null} style={{ background: '#fff' }}>
          <Menu
            mode="inline"
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            style={{ height: '100%' }}
          
          >
            <SubMenu key="sub1" title={<span><Icon type="user" />subnav 1</span>}>
              <Menu.Item key="1">option1</Menu.Item>
              <Menu.Item key="2">option2</Menu.Item>
              <Menu.Item key="3">option3</Menu.Item>
              <Menu.Item key="4">option4</Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" title={<span><Icon type="laptop" />subnav 2</span>}>
              <Menu.Item key="5">option5</Menu.Item>
              <Menu.Item key="6">option6</Menu.Item>
              <Menu.Item key="7">option7</Menu.Item>
              <Menu.Item key="8">option8</Menu.Item>
            </SubMenu>
            <SubMenu key="sub3" title={<span><Icon type="notification" />subnav 3</span>}>
              <Menu.Item key="9">option9</Menu.Item>
              <Menu.Item key="10">option10</Menu.Item>
              <Menu.Item key="11">option11</Menu.Item>
              <Menu.Item key="12">option12</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
      </Layout>
      
    </Content>
    
    <Footer style={{ textAlign: 'center' }}>
      Ant Design ©2018 Created by Ant UED
    </Footer>
  </Layout> */}

 {/* <Layout>
    <Header className="header">
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['2']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="2">nav 2</Menu.Item>
        <Menu.Item key="3">nav 3</Menu.Item>
      </Menu>
    </Header>
    <Layout>
    <Sider  width={280}  breakpoint="lg"
      collapsedWidth="0" trigger={null} reverseArrow={true}>
        <Menu
         theme="dark"
          mode="inline"
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          style={{ height: '100%', borderRight: 10 }}
        >
          <SubMenu key="sub1" title={<span><Icon type="user" />subnav 1</span>}>
            <Menu.Item key="1">option1</Menu.Item>
            <Menu.Item key="2">option2</Menu.Item>
            <Menu.Item key="3">option3</Menu.Item>
            <Menu.Item key="4">option4</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" title={<span><Icon type="laptop" />subnav 2</span>}>
            <Menu.Item key="5">option5</Menu.Item>
            <Menu.Item key="6">option6</Menu.Item>
            <Menu.Item key="7">option7</Menu.Item>
            <Menu.Item key="8">option8</Menu.Item>
          </SubMenu>
          <SubMenu key="sub3" title={<span><Icon type="notification" />subnav 3</span>}>
            <Menu.Item key="9">option9</Menu.Item>
            <Menu.Item key="10">option10</Menu.Item>
            <Menu.Item key="11">option11</Menu.Item>
            <Menu.Item key="12">option12</Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 0px 0px' }}>
       
        
        <Content style={{ background: '#fff', paddingTop: 20,paddingBottom:84,paddingLeft:20,paddingRight:20, marginTop: 0, minHeight: 800 }}>
        <Breadcrumb style={{ margin: '16px 0',fontSize:'17px' }}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>{superBlock}</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
          <h1>Monuments, Building and Other Structures</h1>
         <p  style={{ fontSize:'18px', lineHeight:'1.65' }}>Hi <code>helojfisjf</code> ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkh</p>
         <h1>Monuments, Building and Other Structures</h1>
         <p  style={{ fontSize:'18px', lineHeight:'1.65' }}>Hi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkh</p>

         <h1>Monuments, Building and Other Structures</h1>
         <p  style={{ fontSize:'18px', lineHeight:'1.65' }}>Hi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkhHi helojfisjf ekjf ewjfjkewf kjew fkebwfkewkfb wekhbf hewbf khebf khebfkh</p>
         <Row gutter={16}>
      <Col xs={24} sm={12} md={12} lg={12} xl={12} style={{marginBottom:"10px"}} ><Button style={{height:'auto',padding:10}} type="primary" block>Primary</Button></Col>
      <Col  xs={24} sm={12} md={12} lg={12} xl={12}><Button type="primary" block>Primary</Button></Col>
    </Row>
        </Content>
       
      </Layout>
     
    </Layout>
  </Layout> */}
      {/* <FullWidthRow> */}
        {/* <div
          className='intro-layout'
          dangerouslySetInnerHTML={{ __html: html }}
        /> */}
        {/* <Alert message="Success Text" type="success" /> */}
      {/* </FullWidthRow> */}



{/* <Layout> 
// <Header className="header">
//       <div className="logo" />
//       <Menu
//         theme="dark"
//         mode="horizontal"
//         defaultSelectedKeys={['2']}
//         style={{ lineHeight: '64px' }}
//       >
//         <Menu.Item key="1">nav 1</Menu.Item>
//         <Menu.Item key="2">nav 2</Menu.Item>
//         <Menu.Item key="3">nav 3</Menu.Item>
//       </Menu>
//     </Header>
//     <Layout>
//     <Sider width={250} style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}>
//       <div className="logo" />
//       <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
//         <Menu.Item key="1">
//           <Icon type="user" />
//           <span className="nav-text">nav 1</span>
//         </Menu.Item>
//         <Menu.Item key="2">
//           <Icon type="video-camera" />
//           <span className="nav-text">nav 2</span>
//         </Menu.Item>
//         <Menu.Item key="3">
//           <Icon type="upload" />
//           <span className="nav-text">nav 3</span>
//         </Menu.Item>
//         <Menu.Item key="4">
//           <Icon type="bar-chart" />
//           <span className="nav-text">nav 4</span>
//         </Menu.Item>
//         <Menu.Item key="5">
//           <Icon type="cloud-o" />
//           <span className="nav-text">nav 5</span>
//         </Menu.Item>
//         <Menu.Item key="6">
//           <Icon type="appstore-o" />
//           <span className="nav-text">nav 6</span>
//         </Menu.Item>
//         <SubMenu key="sub2" title={<span><Icon type="user" />subnav 1</span>}>
//             <Menu.Item key="1">option1</Menu.Item>
//             <Menu.Item key="2">option2</Menu.Item>
//             <Menu.Item key="3">option3</Menu.Item>
//             <Menu.Item key="4">option4</Menu.Item>
//           </SubMenu>
//         <SubMenu key="sub1" title={<span><Icon type="user" />subnav 1</span>}>
//             <Menu.Item key="1">option1</Menu.Item>
//             <Menu.Item key="2">option2</Menu.Item>
//             <Menu.Item key="3">option3</Menu.Item>
//             <Menu.Item key="4">option4</Menu.Item>
//           </SubMenu>
//       </Menu>
//     </Sider>
//     <Layout style={{ padding: '0 24px 24px' }}>
//       <Content>
//         <div style={{ padding: 24, background: '#fff', textAlign: 'center' }}>
//           ...
//           <br />
//           Really
//           <br />...<br />...<br />...<br />
//           long
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />...
//           <br />...<br />...<br />...<br />...<br />...<br />
//           content
//         </div>
//       </Content>
//       <Footer style={{ textAlign: 'center' }}>
//         Ant Design ©2018 Created by Ant UED
//       </Footer>
//     </Layout>
//   </Layout>

// </Layout> */}





<HeaderTop></HeaderTop>
<div className="main-wrapper" style={{}}>
     <Row>
     <Col xs={{ span: 0, offset: 0 }} sm={{ span: 0, offset: 0 }} md={{ span: 0, offset: 0 }} lg={{ span: 6, offset: 0 }} xl={{ span: 5, offset: 0 }} xxl={{ span: 4, offset: 0 }} style={{background:'white', position:'fixed', height:'100%', zIndex:1,overflow:'auto',marginTop:67,padding:15,borderRight:'1px solid rgb(223, 223, 223)'}} id="sidebarleft">
     <Input className="inputSidebar" placeholder="Search Course" prefix={<Icon type="search" />} />
    
     <Menu
      mode="inline"
      defaultSelectedKeys={['2']}
      defaultOpenKeys={['sub1']}
      style={{marginBottom:100,paddingTop:20}}
      className="aside-container side-menu-site"
    >

      <SubMenu className="side-menu-site" key="sub1" title={<span>Tools to build VR Content</span>}>
       <Menu.Item className="ant-menu-submenu-title-two" key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />Hey Prashanth</span> </Menu.Item>
       <Menu.Item key="1"><span> <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="twoTone" />Hey Prashanth</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />Hey Prashanth</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />Hey Prashanth</span> </Menu.Item>
       </SubMenu>

         <SubMenu key="sub2" title={<span>subnav 1</span>}>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span> <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="twoTone" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       </SubMenu>
       <SubMenu key="sub3" title={<span>subnav 1</span>}>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span> <Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="twoTone" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       <Menu.Item key="1"><span><Icon type="right-circle" theme="outlined" />option1</span> </Menu.Item>
       </SubMenu>
       <Menu.Item className="ant-menu-submenu-title" key="1"><span>Hey hbhvb</span> </Menu.Item>
       <Menu.Item className="ant-menu-submenu-title" key="1"><span>Hey hbhvb</span> </Menu.Item>
       <Menu.Item className="ant-menu-submenu-title" key="1"><span>Hey hbhvb</span> </Menu.Item>
       <Menu.Item className="ant-menu-submenu-title" key="1"><span>Hey hbhvb</span> </Menu.Item>

    </Menu>
     </Col>
  
     <Col xs={{ span: 24, offset: 0 }} sm={{ span: 24, offset: 0 }} md={{ span: 24, offset: 0 }} lg={{ span: 18, offset: 6 }} xl={{ span: 19, offset: 5 }} xxl={{ span: 20, offset: 4 }} style={{marginTop:67}}> 

   
   
     <Code challengeId="2" />
     <Code challengeId ="3"/> 
     
     </Col>
   
    </Row>

    {/* </Row>
        <Row style={{marginTop:65}}>

            <Col xxl={4} xl={5} lg={6} md={0} sm={0} xs={0}  style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }} className="main-menu">
            <Menu
      
        mode="inline"
        defaultSelectedKeys={['2']}
        defaultOpenKeys={['sub1']}
        inlineIndent="40"
        className="aside-container menu-site"
        mode="inline"
      >


        <Menu.Item key="1">VR 101</Menu.Item>
        <Menu.Item key="2">Getting Started</Menu.Item>
        <Menu.Item key="3">Unity Primitives and Transform controls</Menu.Item>
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="1">VR 101</Menu.Item>
        <Menu.Item key="2">Getting Started</Menu.Item>
        <Menu.Item key="3">Unity Primitives and Transform controls</Menu.Item>
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="1">VR 101</Menu.Item>
        <Menu.Item key="2">Getting Started</Menu.Item>
        <Menu.Item key="3">Unity Primitives and Transform controls</Menu.Item>
        <Menu.Item key="1">nav 1</Menu.Item>
        <SubMenu key="sub1" title={<span><Icon type="user" />subnav 1</span>}>
          <Menu.Item key="1">option1</Menu.Item>
          <Menu.Item key="2">option2</Menu.Item>
          <Menu.Item key="3">option3</Menu.Item>
           <Menu.Item key="4">option4</Menu.Item>
         </SubMenu>
      </Menu>
              </Col>

                <Col xxl={20} xl={19} lg={18} md={24} sm={24} xs={24} offset = {5} style={{height:1600}} >
                <h1>Hello</h1>
          </Col>
          </Row> */}
          </div>

    </Fragment>



  );
}

SuperBlockIntroductionPage.displayName = 'SuperBlockIntroductionPage';
SuperBlockIntroductionPage.propTypes = propTypes;

export default SuperBlockIntroductionPage;

export const query = graphql`
  query SuperBlockIntroPageBySlug($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        superBlock
      }
      html
    }
  }
`;
